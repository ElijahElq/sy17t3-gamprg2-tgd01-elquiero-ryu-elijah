﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Elevator : MonoBehaviour {

    public bool ElevReady = false;
    public GameObject Fade;

    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        MoveElevator();
	}

    void MoveElevator()
    {
        if (ElevReady == true)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - Time.deltaTime, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            ElevReady = true;
            StartCoroutine(FadingOut());
        }
    }

    IEnumerator FadingOut()
    {
        yield return new WaitForSeconds(2.0f); 
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("TestScene"))
        {
            yield return new WaitForSeconds(2.0f);
            Fade.GetComponent<Animator>().Play("FadingOutLevel1");
            yield return new WaitForSeconds(2.0f);
            SceneManager.LoadScene("TestScene2");
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("TestScene2"))
        {
            Fade.GetComponent<Animator>().Play("FadingOut");
            yield return new WaitForSeconds(13.0f);
            ElevReady = !ElevReady;
        }

    }
}
