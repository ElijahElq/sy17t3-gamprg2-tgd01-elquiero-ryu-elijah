﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableGrounds : MonoBehaviour {

    public int groundBreakLimit;
    public GameObject[] Grounds;

    private WeightedBox box;

	// Use this for initialization
	void Start () {
        groundBreakLimit = 2;
	}
	
	// Update is called once per frame
	void Update () {

        if (groundBreakLimit == 1)
        {
            Grounds[1].SetActive(true);
            Grounds[0].SetActive(false);
        }
        else if (groundBreakLimit == 2)
        {
            Grounds[0].SetActive(true);
            Grounds[1].SetActive(false);
        }
        else if (groundBreakLimit == 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<WeightedBox>() != null)
        {
            Destroy(collision.gameObject.GetComponent<WeightedBox>());
            groundBreakLimit -= 1;
        }
    }
}
