﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour {

    public AudioMixer audioMixer;

    void Start()
    {
        FindObjectOfType<AudioManager>().Play("BackgroundMusic");
    }
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        FindObjectOfType<AudioManager>().Play("Clicked");
        Screen.fullScreen = isFullscreen;
    }
}
