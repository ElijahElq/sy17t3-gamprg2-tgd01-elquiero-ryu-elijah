﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFG : MonoBehaviour {

    public float Offset;
    public GameObject[] ObjToSpawn;
    public GameObject Prefab;
    public GameObject Parent;

    private int repeatSpawn = 20;
    
    
	// Use this for initialization
	void Start () {
       Invoke("SpawnForeground", 1f);
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    void SpawnForeground()
    {
        for (int i = 0; i < repeatSpawn; i++)
        {
            Prefab = Instantiate(Prefab, new Vector3(Prefab.transform.position.x + 23.31f, Prefab.transform.position.y, Prefab.transform.position.z), Quaternion.identity) as GameObject;

            i++;

            Prefab.transform.SetParent(Parent.transform);
        }
    }
}
