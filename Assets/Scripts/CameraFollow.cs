﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
        FindObjectOfType<AudioManager>().Play("BackgroundMusic");
       
    }

    // Update is called once per frame
    void Update () {

        transform.position = new Vector3(player.transform.position.x + 7.36f, player.transform.position.y + 2.2f, transform.position.z);  
      
    }

   
}
