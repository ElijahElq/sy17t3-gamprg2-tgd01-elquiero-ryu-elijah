﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PlayCutscene : MonoBehaviour {

    public GameObject[] Frames;
    int index = 0;
    bool startGame = false;
    // Use this for initialization
    void Start() {

        FindObjectOfType<AudioManager>().Play("CutsceneMusic");
        Frames[0].SetActive(true);
        StartCoroutine(PlayFrames());
    }

    // Update is called once per frame
    void Update() {

        StartGame();

    }

    void StartGame()
    {
        if (startGame == true)
        {
            Debug.Log("STARAT");
            
            SceneManager.LoadScene("TestScene");
        }
    }
    IEnumerator PlayFrames()
    {
        while (index < Frames.Length)
        {
            yield return new WaitForSeconds(1.0f);
            Frames[index].SetActive(false);
            index += 1;
            if (index < Frames.Length)
            {
                Frames[index].SetActive(true);

            }
            else if (index >= Frames.Length)
            {
                Debug.Log("rARAT");
                Frames[17].SetActive(true);
                startGame = true;
                index = Frames.Length + index;
                
            }
           
        }
    }
}
