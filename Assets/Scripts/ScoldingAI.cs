﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoldingAI : MonoBehaviour {

    public GameObject Player;
    public float Stamina;
    public GameObject GameOver;
    [HideInInspector]
    public bool NoticedPlayer = false;
    public float Speed;
    public GameObject ScolderAI;
    public float distanceChecker;

    private Animator anim;
    private float distance;

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        // Check distance
        if (Vector3.Distance(Player.transform.position, transform.position) < distanceChecker)
        {
           NoticedPlayer = true;
        }

        
        Chase();
        
        //Debug.Log(distance);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            GameOver.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void Chase()
    {
        if (NoticedPlayer == true)
        {

            transform.LookAt(Player.transform.position);
            transform.Rotate(new Vector3(0, 90, 0), Space.Self);

            
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
             anim.Play("ChaseScolder");
            
        }
    }

}
