﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSystem : MonoBehaviour {

    public GameObject[] Subtitles;

	// Use this for initialization
	void Start () {
        for (int i = 0; i > Subtitles.Length; i++)
        {
            Subtitles[i].SetActive(false);
        }
  
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator FirstThoughts()
    {
        Subtitles[0].SetActive(true);
        yield return new WaitForSeconds(2.5f);
        {
            Subtitles[0].SetActive(false);
            Subtitles[1].SetActive(true);
        }
        yield return new WaitForSeconds(2.5f);
        {
            Subtitles[1].SetActive(false);
        }
    }

    public IEnumerator SecondThoughts()
    {
        
        yield return new WaitForSeconds(2.0f);
        {
            Subtitles[2].SetActive(true);
        }
  
    }
}
