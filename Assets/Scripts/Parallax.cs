﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

    public Transform[] Backgrounds;
    private float[] ParallaxScale;
    public float Smoothing = 1f;

    private Transform cam;             
    private Vector3 previousCamPos;    

    void Awake()
    {
        cam = Camera.main.transform;
    }

    // Use this for initialization
    void Start()
    {
        previousCamPos = cam.position;

        ParallaxScale = new float[Backgrounds.Length];
        for (int i = 0; i < Backgrounds.Length; i++)
        {
            ParallaxScale[i] = Backgrounds[i].position.z * -0.2f;
        }
    }

    // Update is called once per frame
    void Update()
    {

        // for each background
        for (int i = 0; i < Backgrounds.Length; i++)
        {
            float parallax = (previousCamPos.x - cam.position.x) * ParallaxScale[i];
            float backgroundTargetPosX = Backgrounds[i].position.x + parallax;

            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, Backgrounds[i].position.y, Backgrounds[i].position.z);
            Backgrounds[i].position = Vector3.Lerp(Backgrounds[i].position, backgroundTargetPos, Smoothing * Time.deltaTime);
        }
        previousCamPos = cam.position;
    }
}
