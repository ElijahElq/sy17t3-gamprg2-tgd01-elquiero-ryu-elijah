﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform Player;

    public float SmoothSpeed = 0.125f;
    public Vector3 OffSet;

	// Use this for initialization
	void Start () {
        FindObjectOfType<AudioManager>().Play("BackgroundMusic");
       
    }

    // Update is called once per frame
    void FixedUpdate () {

        Vector3 desiredPosition = Player.position + OffSet;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, SmoothSpeed * Time.deltaTime);
        transform.position = Player.position + OffSet;
      
    }

   
}
