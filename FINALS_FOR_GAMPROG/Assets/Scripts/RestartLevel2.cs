﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class RestartLevel2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RestartTwo()
    {
        SceneManager.LoadScene("TestScene2");
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }
}
