﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollerBG : MonoBehaviour {

    public Transform RollerPosition;

    private bool reset;
    
	// Use this for initialization
	void Start () {
        reset = false;
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "ResetTransform")
        {
            reset = true;
            transform.position = RollerPosition.position;
        }
    }

}
