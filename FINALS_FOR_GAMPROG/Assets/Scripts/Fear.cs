﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fear : MonoBehaviour {

    public bool isScared = false;
    private int afraidMeter = 100;
    PlayerController control;

    // Use this for initialization
    void Start () {
        control = GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        CheckScared();
	}

    void CheckScared()
    {
        if (afraidMeter <= 0)
        {
            control.enabled = false;
        }
    }

}
