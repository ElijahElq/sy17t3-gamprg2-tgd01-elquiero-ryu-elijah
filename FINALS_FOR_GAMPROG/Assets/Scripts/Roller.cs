﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roller : MonoBehaviour {

    public GameObject SurfaceEffector;
    public int TriggerMechanism;
    public GameObject[] Lights;
    public GameObject[] RollerGround;
    public GameObject ParentRoller;

    private GameObject player;
    //private GameObject[] boxes;

    private bool started;

    BoxCollider2D boxCol;
    PhysicMaterial phys;

	// Use this for initialization
	void Start () {
        started = false;
        TriggerMechanism = 0;
        boxCol = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {

        if (TriggerMechanism == 3)
        {
            started = true;
        }
        StartRoller();
        TurnOnLights();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player" )
        {
            TriggerMechanism += 1;
            player = collision.gameObject;
           
        }

        if (collision.gameObject.tag == "pushable")
        {
            TriggerMechanism += 1;
            collision.gameObject.transform.parent = ParentRoller.transform;

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
   
        if (collision.gameObject.name == "Player")
        {
            player.transform.parent = ParentRoller.transform;
            // player.transform.localPosition = new Vector3(transform.localPosition.x, player.transform.localPosition.y, player.transform.localPosition.z);
        }

        if (collision.gameObject.tag == "pushable")
        {
            GameObject[] boxes = GameObject.FindGameObjectsWithTag("pushable");
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].transform.parent = ParentRoller.transform;
                boxes[i].GetComponent<Collider2D>().sharedMaterial = null;
            }
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player" || collision.gameObject.tag == "pushable")
        {
            TriggerMechanism -= 1;
            Lights[0].SetActive(false);
            Lights[1].SetActive(false);
            Lights[2].SetActive(false);
            collision.gameObject.transform.parent = null;
        }
    }

    void StartRoller()
    {
        if (started == true)
        {
            StartCoroutine(Activate());
        }
        if (started == false)
        {
            transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
        
    }

    void TurnOnLights()
    {
        if (TriggerMechanism == 1)
        {
            Lights[0].SetActive(true);
        }
        if (TriggerMechanism == 2)
        {
            Lights[1].SetActive(true);
        }
        if (TriggerMechanism == 3)
        {
            Lights[2].SetActive(true);
        }
        if (TriggerMechanism == 0)
        {
            Lights[0].SetActive(false);
            Lights[1].SetActive(false);
            Lights[2].SetActive(false);
        }
    }

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(2.0f);
        //SurfaceEffector.GetComponent<SurfaceEffector2D>().enabled = true;
        transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

        transform.position = new Vector3(transform.position.x + 9 * Time.deltaTime, transform.position.y, transform.position.z);
        


    }
}
