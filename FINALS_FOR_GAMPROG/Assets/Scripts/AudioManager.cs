﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;

    // Use this for initialization
    void Awake () {
        foreach (Sound s_sounds in sounds)
        {
            s_sounds.Source = gameObject.AddComponent<AudioSource>();
            s_sounds.Source.clip = s_sounds.Clip;
            s_sounds.Source.volume = s_sounds.Volume;
            s_sounds.Source.pitch = s_sounds.Pitch;
            s_sounds.Source.loop = s_sounds.Loop;
            s_sounds.Source.outputAudioMixerGroup = s_sounds.Mixer;
        }
	}

    public void Play(string name)
    {
        Sound s_sounds = Array.Find(sounds, sound => sound.Name == name);    
        s_sounds.Source.Play();
        
    }

    public void Stop(string name)
    {
        Sound s_sounds = Array.Find(sounds, sound => sound.Name == name);
        s_sounds.Source.Stop();

    }
}
