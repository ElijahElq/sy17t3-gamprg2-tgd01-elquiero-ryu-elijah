﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollisions : MonoBehaviour {

    public GameObject Scolder;
    public GameObject Door;
    public GameObject Camcam;
    public GameObject DialogueSystem;

    private Animator anim;
    private PlayerController control;
    private bool doorOpen = false;
    private float zoomAmount;
    private bool zoomingOut;
    private bool zoomingIn;

    DialogueSystem dialogue;
    ScoldingAI scol;
    BoxCollider2D boxCol;
    PhysicsMaterial2D phys;


    // Use this for initialization
    void Start () {
        scol = Scolder.GetComponent<ScoldingAI>();
        control = GetComponent<PlayerController>();
        anim = GetComponent<Animator>();
        dialogue = DialogueSystem.GetComponent<DialogueSystem>();

        zoomAmount = Camera.main.orthographicSize;
        

        zoomingIn = false;
        zoomingOut = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (doorOpen == true)
        {
            Door.transform.position = new Vector3(Door.transform.position.x, Door.transform.position.y * 2 - Time.deltaTime, Door.transform.position.z);
        }

        ZoomIn();
        ZoomOut();
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground" || collision.gameObject.tag == "pushable")
        {
            anim.Play("Idle");
            control.onGround = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "CameraZoomOut")
        {
            zoomingIn = false;
            zoomingOut = true;

            if (dialogue.gaveFirst == false)
            {
                StartCoroutine(dialogue.FirstThoughts());
                dialogue.gaveFirst = true;
            }
            Debug.Log("Zoom Out");
        }

        if (collision.gameObject.name == "CameraDefault")
        {
            zoomingIn = true;
            zoomingOut = false;

            Debug.Log("Zoom In");
        }

        if (collision.gameObject.name == "CameraZoomOut_2")
        {
            zoomingIn = false;
            zoomingOut = true;

            Debug.Log("Zoom Out");
        }

        if (collision.gameObject.name == "CameraDefault_2")
        {
            zoomingIn = true;
            zoomingOut = false;


            Debug.Log("Zoom In");
        }

        if (collision.gameObject.name == "DoneTrigger")
        {

            SceneManager.LoadScene("Done");

            Debug.Log("Zoom In");
        }

        if (collision.gameObject.name == "JumpTutorialTrigger")
        {
            control.taughtJump = true;
            control.Tutorials[1].SetActive(true);
            Time.timeScale = 0;
            Destroy(control.TutorialTriggers[0]);
            control.canPressEnter = true;
        }

        if (collision.gameObject.name == "PushTutorialTrigger")
        {
            control.Tutorials[2].SetActive(true);
            Time.timeScale = 0;
            Destroy(control.TutorialTriggers[1]);
            control.canPressEnter = true;
        }

        if (collision.gameObject.name == "ScolderTrigger")
        {
            scol.NoticedPlayer = true;
        }

        if (collision.gameObject.name == "WoodIntroTrigger")
        {
            control.Tutorials[3].SetActive(true);
            Time.timeScale = 0;
            Destroy(control.TutorialTriggers[2]);
            control.canPressEnter = true;
            control.woodIntro = true;
        }

        if (collision.gameObject.name == "SneakTrigger")
        {
            control.Tutorials[4].SetActive(true);
            Time.timeScale = 0;
            Destroy(control.TutorialTriggers[3]);
            control.canPressEnter = true;
        }

        if (collision.gameObject.name == "CameraDefault_0")
        {
            zoomingIn = true;
            zoomingOut = false;


            Debug.Log("Zoom In");
        }


        if (collision.gameObject.name == "CameraZoomPan")
        {
            zoomingIn = false;
            zoomingOut = true;

            //Camcam.transform.position = new Vector3(Camcam.transform.position.x - 14.72f, Camcam.transform.position.y, Camcam.transform.position.z);
           
            Debug.Log("Zoom Out");
        }

        if (collision.gameObject.name == "SecondThoughtTrigger")
        {
            StartCoroutine(dialogue.SecondThoughts());
        }

        if (collision.gameObject.name == "CameraZoomInBack")
        {
            zoomingIn = true;
            zoomingOut = false;


            Debug.Log("Zoom In");
        }

        if (collision.gameObject.name == "CluesTrigger" && dialogue.gaveClue == false)
        {
            dialogue.gaveClue = true;
            StartCoroutine(dialogue.Clue());
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "ElevatorTrigger")
        {
            Debug.Log("1");
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("2");
                Door.SetActive(false);
                //StartCoroutine(ElevOpen());
            }
            //boxCol.sharedMaterial = null;

        }
    }

    IEnumerator ElevOpen()
    {
        doorOpen = true;
        yield return new WaitForSeconds(1.5f);
        Door.SetActive(false);
    }

    void ZoomOut()
    {
        if (zoomingOut == true && zoomingIn == false)
        {
            zoomAmount = zoomAmount + 0.8f * Time.deltaTime;
            Camera.main.orthographicSize = zoomAmount;
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 5, 10);
        }

    }

    void ZoomIn()
    {
        if (zoomingOut == false && zoomingIn == true)
        {
            zoomAmount = zoomAmount - 0.8f * Time.deltaTime;
            Camera.main.orthographicSize = zoomAmount;
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 5, 10);
        }

    }

}
