﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour {

    public GameObject ai;
    SleepingAI woke;


    // Use this for initialization
    void Start () {
        woke = ai.GetComponent<SleepingAI>();
    }
	
	// Update is called once per frame
	void Update () {

        if (woke.Awake == true)
        {
            StartCoroutine(StandingUp());
            
        }
	}

    IEnumerator StandingUp()
    {
        yield return new WaitForSeconds(2.0f);

        transform.position = new Vector3(transform.position.x - 6 * Time.deltaTime, transform.position.y, transform.position.z);
    }

    
}
