﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGuyAI : MonoBehaviour {

    public bool GoingRight = true;
    public bool GoingLeft = false;
    public float Speed;
    public GameObject GameOver;
    public bool ChangeDirection = false;

    private bool faceRight = true;

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GoingRight == true)
        {
            WalkRight();
        }
        if (GoingLeft == true)
        {
            WalkLeft();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    { 
        if (collision.gameObject.name == "Player")
        {
            GameOver.SetActive(true);
            Time.timeScale = 0;
        }

        if (collision.gameObject.name == "PointLeft")
        {

            GoingRight = true;
            GoingLeft = false;
            FaceRight();
        }
        if (collision.gameObject.name == "PointRight")
        {
            GoingLeft = true;
            GoingRight = false;
            FaceLeft();
        }
    }
   

    void WalkRight()
    {
        transform.position = new Vector3(transform.position.x + Speed * Time.deltaTime, transform.position.y, transform.position.z);
    }

    void WalkLeft()
    {
        transform.position = new Vector3(transform.position.x + -Speed * Time.deltaTime, transform.position.y, transform.position.z);
    }

    void FaceRight()
    {
        if (faceRight == false)
        {
            Vector3 playerScale = transform.localScale;
            playerScale.x *= playerScale.x;
            transform.localScale = playerScale;
            faceRight = true;
            WalkRight();
        }
    }

    void FaceLeft()
    {
        if (faceRight == true)
        {
            Vector3 playerScale = transform.localScale;
            playerScale.x *= -1;
            transform.localScale = playerScale;
            faceRight = false;
            WalkLeft();
        }
    }
}
