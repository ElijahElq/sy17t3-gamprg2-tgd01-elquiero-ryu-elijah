﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private Scene scene;

    // Use this for initialization
    void Start () {
        FindObjectOfType<AudioManager>().Play("BackgroundMusic");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayGame()
    {
        FindObjectOfType<AudioManager>().Play("Clicked");
        StartCoroutine(GoPlay());
    }

    public void Retry()
    {
        Debug.Log("RESTART FUKC");
        Application.LoadLevel(scene.name);
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void OpenSettings()
    {
        FindObjectOfType<AudioManager>().Play("Clicked");
        StartCoroutine(GoSettings());
       
    }
    public void GotoMainMenu()
    {
        FindObjectOfType<AudioManager>().Play("Clicked");
        Debug.Log("MIANMENU FUKC");
        StartCoroutine(GoMenu());
    }

    IEnumerator GoSettings()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Settings");
    }

    IEnumerator GoPlay()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Cutscenes");
    }

    IEnumerator GoMenu()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainMenu");
    }
}
