﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepingAI : MonoBehaviour {

    public GameObject AISleeper;
    public GameObject Player;
    [HideInInspector]
    public bool Awake = false;
    public GameObject GameOver;
    public float Speed;

    private float defaultSpeed = 8;
    private Animator anim;
    private bool standing = false;
    private float distance;

    PlayerController playerSpeed;
    Fear afraid;
    BoxCollider2D ai;

	// Use this for initialization
	void Start () {
        playerSpeed = Player.GetComponent<PlayerController>();
        ai = AISleeper.GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        afraid = Player.GetComponent<Fear>();

        playerSpeed.Speed = defaultSpeed;

    }
	
	// Update is called once per frame
	void Update () {

        CheckDistance();
        Chase();

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (playerSpeed.TotalSpeed == defaultSpeed)
            {
                //Debug.Log("LALALALAL");
                afraid.isScared = true;
                playerSpeed.Speed = 0;
                playerSpeed.TotalSpeed = 0;
                Awake = true;
                StartCoroutine(WakingUp());

            }

            else if (playerSpeed.TotalSpeed == 0)
            {
                return;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (playerSpeed.TotalSpeed == defaultSpeed)
            {
                //Debug.Log("LALALALAL");
                afraid.isScared = true;
                playerSpeed.Speed = 0;
                playerSpeed.TotalSpeed = 0;
                Awake = true;
                StartCoroutine(WakingUp());

            }
        }
    }

    void Chase()
    {
        if (standing == true && Player.transform.position.x > transform.position.x)
        {
            transform.LookAt(Player.transform.position);
            transform.Rotate(new Vector3(0, 90, 0), Space.Self);

            transform.position = new Vector3(AISleeper.transform.position.x + Speed * Time.deltaTime, AISleeper.transform.position.y, AISleeper.transform.position.z);
            anim.Play("Walking");
            Debug.Log(distance);
        }

        if (standing == true && Player.transform.position.x < transform.position.x)
        {
            transform.LookAt(Player.transform.position);
            transform.Rotate(new Vector3(0, 90, 0), Space.Self);


            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
            anim.Play("Walking");
            Debug.Log(distance);
        }
    }

    void CheckDistance()
    {
        distance = Vector3.Distance(Player.transform.position, transform.position);
        if(distance < 3.05f && Awake == true)
        {
            GameOver.SetActive(true);
            Time.timeScale = 0;
        }
    }

    IEnumerator WakingUp()
    {
       
        anim.Play("WakingUp");
        yield return new WaitForSeconds(2.0f);
        standing = true;

    }


}
