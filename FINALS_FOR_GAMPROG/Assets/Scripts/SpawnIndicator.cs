﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnIndicator : MonoBehaviour {

    public Transform Player;

    public GameObject Indicator;

    bool spawning = true;
    GameObject Spawned;
    GrabMechanic obj;
	// Use this for initialization
	void Start () {
        obj = Player.GetComponent<GrabMechanic>();
    }
	
	// Update is called once per frame
	void Update () {

        CheckDistance();
    }

    void CheckDistance()
    {
        if (spawning == true && Vector3.Distance(Player.transform.position, transform.position) < 3 && obj.Grabbed == false)
        {
            Spawned = Instantiate(Indicator, new Vector3(transform.position.x, transform.position.y + 2.62f, transform.position.z), Quaternion.identity);
            Spawned.transform.parent = transform;

            StartCoroutine(DestroyInd());

            spawning = false;

        }

    }

    IEnumerator DestroyInd()
    {

        yield return new WaitForSeconds(1.0f);
        Destroy(Spawned);
        spawning = false;

        yield return new WaitForSeconds(3.0f);
        spawning = true;

        
    }
}
