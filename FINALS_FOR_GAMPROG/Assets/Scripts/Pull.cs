﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pull : MonoBehaviour {

    public bool beingPushed;
    float posX;
    public bool canCarry = true;

	// Use this for initialization
	void Start () {

        posX = transform.position.x;

        
    }
	
	// Update is called once per frame
	void Update () {

        if (canCarry == false)
        {
            transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
        }

        if (beingPushed == false)
        {
            transform.position = new Vector3(posX, transform.position.y);
        }

        else
        {
            posX = transform.position.x;
        }
	}
}
