﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float JumpForce; // Jump force of player
    public float Speed; // Speed of player
    public bool faceRight; // If the player is facing right
    public GameObject WorldUI; // World UI for can and cannot carry
    public float TotalSpeed; // Current speed of player
    public float SneakSpeed; // Speed for sneaking
    public Transform[] StartingPosition; // Player starting position (can work for check points)
    public GameObject[] Tutorials; // Array of tutorial panels
    public GameObject[] TutorialTriggers; // Triggers for tutorial activation
    public GameObject MainMenuIcon; // Main menu icon
    public bool StartGame = false;

    private Vector3 velocity; // Player's velocity
    private Rigidbody2D rb; // Rigidbody variable
    private Animator anim; // Animator variable
    private Fear afraid; // Fear system (not yet used)
    private Scene scene; // Scene variable
    private bool sneaking = false; // Bool for if player is currently sneaking
    

    [HideInInspector]
    public bool onGround = true; // Bool for if player is on the ground
    [HideInInspector]
    public bool taughtJump = false; // Bool for enabling jump after tutorial
    [HideInInspector]
    public bool woodIntro = false; // Bool for wood is introduced or not
    [HideInInspector]
    public bool canPressEnter = true; // Bool for closing tutorials and playing the click sound

    GrabMechanic obj; // For grabbing bool checking

    // Use this for initialization
    void Start()
    {
        if (StartGame == false)
        {
            Time.timeScale = 0; // Timescale to 0 because of tutorial
            taughtJump = false;
        }
        else
        {
            Time.timeScale = 1;
            taughtJump = true;
        }

        transform.position = StartingPosition[0].position; // Setting starting position 
                                                           //(Just comment out if you want player to start at diff location for testing)

        //Instantiates
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        obj = GetComponent<GrabMechanic>();
        afraid = GetComponent<Fear>();
        scene = SceneManager.GetActiveScene();

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("TestScene"))
            Tutorials[0].SetActive(true); // Activate move tutorial
    }

    // Update is called once per frame
    void Update()
    {
        Movements(); // Movements' controls function
        Restart(); // Restart if gameover function
        TutorialExit(); // Exiting tutorial

        if (canPressEnter == true)
        {
            // Hiding Main Menu icon when tutorial is appearing
            MainMenuIcon.SetActive(false);
        }
        else
        {
            // Activate after exiting tutorial
            MainMenuIcon.SetActive(true);
        }
    }

   

    void FaceRight()
    {
        // Facing right (Time.timeScale so that player can't turn around when tutorials are active) 
        // Face right if there's no grabbed object

        if (faceRight == false && obj.Grabbed == false && Time.timeScale == 1)
        {
            Vector3 playerScale = transform.localScale; // Instatiate local scale of the player (For facing right or left)
            Vector3 uiScale = WorldUI.transform.localScale; // UI local scale set via player
           

            playerScale.x *= playerScale.x; // Setting local scale to face right
            transform.localScale = playerScale; // Setting local scale of the current player scale
            faceRight = true; // Bool for various application
        }
    }

    public void FaceLeft()
    {
        // Same as facing right but with different values

        if (faceRight == true && obj.Grabbed == false && Time.timeScale == 1)
        {
            Vector3 playerScale = transform.localScale;
            Vector3 uiScale = WorldUI.transform.localScale;

            playerScale.x *= -1; // -1 to face left
            transform.localScale = playerScale;
            faceRight = false;
            
        }
    }

    void Sneak()
    {
        // Holding S to sneak
        if (Input.GetKeyDown(KeyCode.S))
        {
            SneakSpeed = Speed / 2; // Speed reducing by half
            Speed = SneakSpeed; // Setting sneakspeed to speed so (we can set back default speed)
            sneaking = true; // Bool for various application
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            Speed = SneakSpeed * 2; // Setting default speed by multiplying back the reduced half speed
            sneaking = false; // Bool for various application

        }

    }

    void Movements()
    {
        // Input of sneaking (Seperated so we can edit easily the sneak mechanic)
        Sneak();
        
        // Setting the current speed
        TotalSpeed = Speed;

        // Jump (If on the ground, is not scared, if there's no object grabbed, and the tutorial of jump is done)

        if (Input.GetKeyDown(KeyCode.J) && onGround == true && afraid.isScared != true && obj.Grabbed == false && taughtJump == true)
        {
            rb.velocity = new Vector3(0, JumpForce, 0); // Rigidbody velocity applied jump force
            anim.Play("Jump"); // Jump animation
            onGround = false; // Setting on the ground no more

        }

        velocity = rb.velocity; // Setting rigidbody velocity to velocity

        /////////////////////////////////////****************************
        ////////////////////////////////////******************************
        /////////////////////////////////////****************************
        ////////////////////////////////////******************************

        // Go RIGHT (if not scared or disabled controls)
        if (Input.GetKey(KeyCode.D) && afraid.isScared != true)
        {
            velocity.x = Speed; // Setting speed to velocity x
            FaceRight(); // Calling function to face right
           

            // If there is no grabbed object, on the ground, and not sneaking play PLAYERWALK)
            if (obj.Grabbed == false && onGround == true && sneaking == false)
            {
                anim.Play("PlayerWalk");             
            }

            // If there is grabbed object, facing right while going right, and on the ground, play PUSHING)
            else if (obj.Grabbed == true && faceRight == true && onGround == true)
            {
                anim.Play("Pushing");
            }

            // If there is grabbed object, not facing right (facing left) while going right, and on the ground, play PUllING)
            else if (obj.Grabbed == true && faceRight == false && onGround == true) 
            {
                anim.Play("Pulling");
            }

            // If there is no grabbed object, facing right while going right, on the ground, and is sneaking play SNEAKING)
            else if (obj.Grabbed == false && faceRight == true && onGround == true && sneaking == true)
            {
                anim.Play("Sneaking");
            }
        }

        // Stopping
        else if (Input.GetKeyUp(KeyCode.D))
        {
            velocity.x = 0; // Setting velocity to zero
            FaceRight(); // Calling FacerRight
            FindObjectOfType<AudioManager>().Stop("WoodGrab"); // Stopping WOODGRAB sound effect when stopping while there is grabbed object

            // if on ground, no grabbed object, and player stopped play IDLE
            if (onGround == true && obj.Grabbed == false)
            {
                anim.Play("Idle");
            }
        }

        // If going right while there is grabbed object, and moving the object play WOODGRAB sound effect
        if (Input.GetKeyDown(KeyCode.D) && obj.Grabbed == true && obj.canPush == true)
        {
            FindObjectOfType<AudioManager>().Play("WoodGrab");
        }


        /////////////////////////////////////****************************
        ////////////////////////////////////******************************
        /////////////////////////////////////****************************
        ////////////////////////////////////******************************


        // Go LEFT (if not scared or disabled controls)
        if (Input.GetKey(KeyCode.A) && afraid.isScared != true)
        {
            velocity.x = -Speed; // Setting speed to velocity x (negative because its going left)
            FaceLeft(); // Calling function to face right

            // If there is no grabbed object, on the ground, and not sneaking play PLAYERWALK)
            if (obj.Grabbed == false && onGround == true && sneaking == false)
            {
                anim.Play("PlayerWalk");
            }

            // If there is grabbed object, facing left while going left, and on the ground, play PUSHING)
            else if (obj.Grabbed == true && faceRight == false && onGround == true)
            {
                anim.Play("Pushing");
            }

            // If there is grabbed object, not facing left (facing right) while going left, and on the ground, play PULLING)
            else if (obj.Grabbed == true && faceRight == true && onGround == true)
            {
                anim.Play("Pulling");
            }

            // If there is no grabbed object, facing left while going left, on the ground, and is sneaking play SNEAKING)
            else if (obj.Grabbed == false && faceRight == false && onGround == true && sneaking == true)
            {
                anim.Play("Sneaking");
            }

        }

        // Stopping
        else if (Input.GetKeyUp(KeyCode.A))
        {
            velocity.x = 0; // Setting velocity to zero
            FaceLeft(); // Calling face left
            FindObjectOfType<AudioManager>().Stop("WoodGrab"); // Stopping WOODGRAB sound effect when stopping while there is grabbed object

            // if on ground, no grabbed object, and player stopped play IDLE
            if (onGround == true && obj.Grabbed == false)
            {
                anim.Play("Idle");
            }
        }

        // If going left while there is grabbed object, and moving the object play WOODGRAB sound effect
        if (Input.GetKeyDown(KeyCode.A) && obj.Grabbed == true && obj.canPush == true)
        {
            FindObjectOfType<AudioManager>().Play("WoodGrab");
        }

        rb.velocity = velocity; // Setting velocity to rigidbody velocity smooth out velocities

        // If pressing interact while the grabbed object is cannot be move or no object to grab play HOLDING
        if (Input.GetKeyDown(KeyCode.H) && obj.Grabbed == false)
        {
            anim.Play("Holding");
        }
        // Letting go or going back to idle
        if (Input.GetKeyUp(KeyCode.H) && obj.Grabbed == false)
        {
            anim.Play("Idle");
        }

    }

    // Backspace to Restart
    void Restart()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            Application.LoadLevel(scene.name);

            Debug.Log("Restart");
        }
    }

    // Clicking main menu icon
    public void MainMenu()
    {
        FindObjectOfType<AudioManager>().Play("Clicked");
        StartCoroutine(GoMain());
       
    }

    // Closing tutorials
    void TutorialExit()
    {
        // Bool can enter so if enter is pressed while there is no tutorail click sound cannot be played
        if (Input.GetKeyDown(KeyCode.Return) && canPressEnter == true && StartGame == false)
        {
            FindObjectOfType<AudioManager>().Play("Clicked");
            canPressEnter = false; // Setting to false so enter is not gonna play sound while no tutorial

            //Closing all tutorals
            Tutorials[0].SetActive(false);
            Tutorials[1].SetActive(false);
            Tutorials[2].SetActive(false);
            Tutorials[3].SetActive(false);
            Tutorials[4].SetActive(false);

            Time.timeScale = 1; // Resuming game
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("TestScene2"))
            {
                StartGame = true;
            }
            // Zooming out a little bit in wood introduction
            if (woodIntro == true)
            {
                Camera.main.orthographicSize = 6; // Default is 5
            }
        }
    }

    // Go main menu with delay so the clicked sound can be played
    IEnumerator GoMain()
    {
        yield return new WaitForSeconds(0.2f);
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

}
