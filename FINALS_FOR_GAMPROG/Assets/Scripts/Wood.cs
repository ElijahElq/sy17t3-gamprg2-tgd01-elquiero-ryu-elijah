﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour {

    public float Delay;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            Debug.Log("HGIT");
            StartCoroutine(Fall());
            StartCoroutine(Shake());

        }

        if (collision.gameObject.tag == "ground" || collision.gameObject.tag == "pushable")
        {
            Debug.Log("Stroy");
            FindObjectOfType<AudioManager>().Play("WoodBreak");
            Destroy(gameObject);

        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    IEnumerator Fall()
    {

        yield return new WaitForSeconds(Delay);
        gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

    }

    IEnumerator Shake()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 0.05f);
        yield return new WaitForSeconds(0.05f);
        transform.position = new Vector2(transform.position.x, transform.position.y + 0.05f);
    }
}
