﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabMechanic : MonoBehaviour {

    public float distance = 1.0f;
    public LayerMask boxMask;
    public bool Grabbed = false;
    public bool canPush;

    public GameObject[] ui;

    private Animator anim;

    GameObject box;
    Pull pushable;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Grab();

    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);

    }

    void Grab()
    {
        pushable = GetComponent<Pull>();

        Physics2D.queriesStartInColliders = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distance, boxMask);

        if (hit.collider != null && hit.collider.gameObject.tag == "pushable" && Input.GetKeyDown(KeyCode.H))
        {
            box = hit.collider.gameObject;
            Grabbed = true;
            Debug.Log(Grabbed);

            if (box.GetComponent<Pull>().canCarry == true && box != null)
            {
                box.GetComponent<FixedJoint2D>().connectedBody = this.GetComponent<Rigidbody2D>();
                box.GetComponent<Pull>().beingPushed = true;

                box.GetComponent<FixedJoint2D>().enabled = true;
                box.GetComponent<Rigidbody2D>().mass = 0.1f;
                Grabbed = true;

                canPush = true;

                ui[0].SetActive(true);

            }

            if (box.GetComponent<Pull>().canCarry == false)
            {
                ui[1].SetActive(true);
                canPush = false;

            }


            StartCoroutine(HideUI());
        }


        else if (Input.GetKeyUp(KeyCode.H) && hit.collider != null)
        {
            box.GetComponent<FixedJoint2D>().enabled = false;
            box.GetComponent<Pull>().beingPushed = false;
            box.GetComponent<Rigidbody2D>().mass = 3;
            Grabbed = false;
        }

        if (hit.collider == null && Input.GetKeyDown(KeyCode.H)) //&& hit.collider.gameObject.tag != "pushable"
        {
            return;
        }

        if (hit.collider == null && Input.GetKeyUp(KeyCode.H))
        {
            Grabbed = false;
            return;
        }
    }

    IEnumerator HideUI()
    {
        yield return new WaitForSeconds(1);

        ui[0].SetActive(false);
        ui[1].SetActive(false);
    }
}
